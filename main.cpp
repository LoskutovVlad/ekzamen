#include <iostream>
#include <vector>
#include <string>

using namespace std;

bool check_let(vector <string> letter, vector <string> newspaper){
    unsigned int result=0;
    vector <bool> used;
    for(unsigned i=0;i<newspaper.size();i++) used.push_back(false);
    for(unsigned int i=0;i<letter.size();i++){
        for(unsigned int j=0; j<newspaper.size(); j++){
            if(letter[i]==newspaper[j]&&used[j]==false){
                result=result+1;
                used[j]=true;
                j=newspaper.size();
            }
        }
    }
    if(result==letter.size()) return true;
    else return false;
}

int main()
{
    bool result = false;
    vector <string> letter{"kill","kill","i","you"};
    vector <string> newspaper1{"i","like", "this", "very", "much"};
    vector <string> newspaper2{"i", "will", "like", "you", "very", "much", "will", "not", "kill"};
    vector <string> newspaper3{"i", "will", "like", "you", "very", "such", "cool", "not", "sit"};

    result = check_let(letter, newspaper1);

    if(result==true)cout<<"newspaper1"<<endl;
    else cout<<"wrong"<<endl;

    result = check_let(letter, newspaper2);

    if(result==true)cout<<"newspaper2"<<endl;
    else cout<<"wrong"<<endl;

    result = check_let(letter, newspaper3);

    if(result==true)cout<<"newspaper3"<<endl;
    else cout<<"wrong"<<endl;

    return 0;
}
